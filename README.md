# Weather provider #

## Task ##

Implement multimodule maven project Weather provider. For getting weather use API 
https://goweather.herokuapp.com/weather/{city}

## Build and Run ##

### Prerequisites ###

- java 8+
- Maven 3.5

### Build project ###

- Clone repository
- Go to the folder
- `mvn clean package`

### Run ###

- `java -jar weather-provider-app/target/weather-provider-app-1.0-SNAPSHOT.jar Tokio`

Please specify your city as an argument here.

Please take into account that used API `https://goweather.herokuapp.com/weather/{city}` is very unpredictable.
It happens that one request can work today but will not work tomorrow (and vice versa).
Sometime the spelling of city for working request can be strange.
So if your request was not successful - try with another city or try to change city spelling.
