package kholod.app;

import kholod.service.WeatherApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WeatherConsoleAppImpl implements WeatherConsoleApp {

    private WeatherApiClient apiClient;

    @Autowired
    public WeatherConsoleAppImpl(WeatherApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public static final int MAX_NUMBER_OF_CHARACTERS_IN_CITY_NAME = 100;
    public static final String ABSENT_ARGUMENT_MESSAGE =
            "The city was not specified. Please provide city name as an argument";
    public static final String INCORRECT_CITY_NAME_MESSAGE =
            "The city name isn't correct. Please provide city name as an argument. It must contain only letters " +
                    "(a-z, A-Z), dash ('-') or whitespace (' ') and be no longer then %d characters";
    public static final String SUCCESSFUL_WEATHER_REQUEST_MESSAGE =
            "Request of weather for city %s was successful.";
    public static final String EXCEPTION_DURING_WEATHER_REQUEST_MESSAGE =
            "Exception occurred while requesting the weather in city %s";
    public static final String EMPTY_RESULT_MESSAGE = "The weather for city %s was not found. Please check its spelling";
    public static final String RESULT_MESSAGE = "The result is: %s";

    @Override
    public boolean validateArguments(String[] args) {
        if (args.length == 0) {
            System.out.println(ABSENT_ARGUMENT_MESSAGE);
            return false;
        }
        return true;
    }

    @Override
    public boolean validateCity(String city) {
        if (city.length() > MAX_NUMBER_OF_CHARACTERS_IN_CITY_NAME || !city.matches("^[a-zA-Z- ]+$")) {
            System.out.println(String.format(INCORRECT_CITY_NAME_MESSAGE, MAX_NUMBER_OF_CHARACTERS_IN_CITY_NAME));
            return false;
        }
        return true;
    }

    @Override
    public String requestWeather(String city) {
        try {
            String result = apiClient.callApi(city.replace(" ", "%20"));
            if (result.isEmpty()) {
                System.out.println(String.format(EMPTY_RESULT_MESSAGE, city));
                return "";
            }
            System.out.println(String.format(SUCCESSFUL_WEATHER_REQUEST_MESSAGE, city));
            return result;
        } catch (Exception e) {
            System.out.println(String.format(EXCEPTION_DURING_WEATHER_REQUEST_MESSAGE, city));
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void printResult(String result) {
        System.out.println(String.format(RESULT_MESSAGE, result));
    }
}
