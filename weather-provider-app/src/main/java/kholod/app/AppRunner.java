package kholod.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements CommandLineRunner {

    private WeatherConsoleApp consoleApp;

    @Autowired
    public AppRunner(WeatherConsoleApp consoleApp) {
        this.consoleApp = consoleApp;
    }

    @Override
    public void run(String... args) {
        System.out.println("**********************************************");
        System.out.println("\n");

        if (!consoleApp.validateArguments(args)) {
            return;
        }
        String city = args[0];
        if (!consoleApp.validateCity(city)) {
            return;
        }
        String result = consoleApp.requestWeather(city);
        if (!result.isEmpty()) {
            consoleApp.printResult(result);
        }
    }
}
