package kholod.app;

public interface WeatherConsoleApp {

    boolean validateArguments(String[] args);

    boolean validateCity(String city);

    String requestWeather(String city);

    void printResult(String result);
}
