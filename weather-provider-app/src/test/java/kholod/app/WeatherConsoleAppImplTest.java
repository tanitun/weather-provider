package kholod.app;

import kholod.service.WeatherApiClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherConsoleAppImplTest {

    private WeatherConsoleAppImpl weatherConsoleApp;

    @Mock
    private WeatherApiClient apiClientMock;

    @Before
    public void init() {
        weatherConsoleApp = new WeatherConsoleAppImpl(apiClientMock);
    }

    @Test
    public void validateArgumentsCheckArgumentsLength() {

        assertTrue(weatherConsoleApp.validateArguments(new String[]{"Dnipro"}));
        assertFalse(weatherConsoleApp.validateArguments(new String[]{}));

    }

    @Test
    public void validateCityCheckLength() {

        StringBuilder city = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            city.append("C");
        }
        assertTrue(weatherConsoleApp.validateCity(city.toString()));
        city.append("C");
        assertFalse(weatherConsoleApp.validateCity(city.toString()));

    }

    @Test
    public void validateCityCheckCharacters() {

        assertTrue(weatherConsoleApp.validateCity("Kyiv"));
        assertFalse(weatherConsoleApp.validateCity("Kyiv2"));

    }

    @Test
    public void requestWeatherReturnResult() throws IOException {
        String expectedResult = "Weather forecast";
        when(apiClientMock.callApi(anyString())).thenReturn(expectedResult);

        String actualResult = weatherConsoleApp.requestWeather("Test City");

        verify(apiClientMock).callApi("Test%20City");
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void requestWeatherReturnEmptyStringWhenWeatherIsNotFound() throws IOException {

        when(apiClientMock.callApi(anyString())).thenReturn("");

        String actualResult = weatherConsoleApp.requestWeather("TestCity");

        verify(apiClientMock).callApi("TestCity");
        assertTrue(actualResult.isEmpty());

    }

    @Test
    public void requestWeatherReturnEmptyStringWhenExceptionIsThrown() throws IOException {

        when(apiClientMock.callApi(anyString())).thenThrow(new RuntimeException());

        String actualResult = weatherConsoleApp.requestWeather("TestCity");

        verify(apiClientMock).callApi("TestCity");
        assertTrue(actualResult.isEmpty());

    }
}