package kholod.app;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AppRunnerTest {

    private AppRunner appRunner;

    @Mock
    private WeatherConsoleApp consoleAppMock;

    @Before
    public void init() {
        appRunner = new AppRunner(consoleAppMock);
    }

    @Test
    public void runWithEmptyArguments() {
        String[] arguments = new String[]{};
        when(consoleAppMock.validateArguments(any())).thenReturn(false);

        appRunner.run();

        verify(consoleAppMock).validateArguments(arguments);
        verify(consoleAppMock, never()).validateCity(anyString());
        verify(consoleAppMock, never()).requestWeather(anyString());
        verify(consoleAppMock, never()).printResult(anyString());
    }

    @Test
    public void runWithIncorrectArguments() {
        String city = "Bad_city_name";
        String[] arguments = new String[]{city};
        when(consoleAppMock.validateArguments(any())).thenReturn(true);
        when(consoleAppMock.validateCity(anyString())).thenReturn(false);

        appRunner.run(city);

        verify(consoleAppMock).validateArguments(arguments);
        verify(consoleAppMock).validateCity(city);
        verify(consoleAppMock, never()).requestWeather(anyString());
        verify(consoleAppMock, never()).printResult(anyString());
    }

    @Test
    public void runAndGetEmptyString() {
        String city = "TestCity";
        String[] arguments = new String[]{city};
        when(consoleAppMock.validateArguments(any())).thenReturn(true);
        when(consoleAppMock.validateCity(anyString())).thenReturn(true);
        when(consoleAppMock.requestWeather(anyString())).thenReturn("");

        appRunner.run(city);

        verify(consoleAppMock).validateArguments(arguments);
        verify(consoleAppMock).validateCity(city);
        verify(consoleAppMock).requestWeather(city);
        verify(consoleAppMock, never()).printResult(anyString());
    }

    @Test
    public void runAndPrintResult() {
        String city = "TestCity";
        String[] arguments = new String[]{city};
        String result = "Weather forecast";
        when(consoleAppMock.validateArguments(any())).thenReturn(true);
        when(consoleAppMock.validateCity(anyString())).thenReturn(true);
        when(consoleAppMock.requestWeather(anyString())).thenReturn(result);

        appRunner.run(city);

        verify(consoleAppMock).validateArguments(arguments);
        verify(consoleAppMock).validateCity(city);
        verify(consoleAppMock).requestWeather(city);
        verify(consoleAppMock).printResult(result);
    }
}
