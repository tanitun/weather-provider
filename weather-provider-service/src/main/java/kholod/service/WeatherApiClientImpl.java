package kholod.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class WeatherApiClientImpl implements WeatherApiClient {

    public static final String WEATHER_API_URL = "https://goweather.herokuapp.com/weather/";

    private HttpRequestHelper httpRequestHelper;
    private ObjectMapper objectMapper;

    @Autowired
    public WeatherApiClientImpl(HttpRequestHelper httpRequestHelper, ObjectMapper objectMapper) {
        this.httpRequestHelper = httpRequestHelper;
        this.objectMapper = objectMapper;
    }

    @Override
    public String callApi(String city) throws IOException {
        String result;
        HttpGet request = new HttpGet(WEATHER_API_URL + city);
        try (CloseableHttpClient httpClient = httpRequestHelper.buildHttpClient();
             CloseableHttpResponse response = httpClient.execute(request)) {
            result = httpRequestHelper.getEntity(response);
        }
        if (!checkResult(result)) {
            return "";
        }
        return result;
    }

    private boolean checkResult(String result) {
        try {
            if (result.isEmpty() || result.contains("NOT_FOUND")) {
                return false;
            }

            String temperature = objectMapper.readTree(result).get("temperature").asText();
            if (temperature.isEmpty()) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
