package kholod.service;

import java.io.IOException;

public interface WeatherApiClient {

    String callApi(String city) throws IOException;
}
