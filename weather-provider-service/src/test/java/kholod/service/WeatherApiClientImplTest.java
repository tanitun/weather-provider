package kholod.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherApiClientImplTest {

    private WeatherApiClient weatherApiClient;

    @Mock
    private HttpRequestHelper httpRequestHelperMock;
    @Mock
    private CloseableHttpClient httpClientMock;
    @Mock
    private CloseableHttpResponse httpResponseMock;
    @Captor
    private ArgumentCaptor<HttpRequestBase> httpRequestArgumentCaptor;

    @Before
    public void init() {
        ObjectMapper objectMapper = new ObjectMapper();
        weatherApiClient = new WeatherApiClientImpl(httpRequestHelperMock, objectMapper);
    }

    @Test
    public void callApiReturnResult() throws Exception {
        String city = "TestCity";
        String expectedResult = "{\"temperature\":\"+8 °C\"}";
        prepareMocks(expectedResult);

        String actualResult = weatherApiClient.callApi(city);

        verifyMethodsCalling(city);
        assertEquals(expectedResult, actualResult);
    }

    @Test(expected = RuntimeException.class)
    public void callApiThrowException() throws Exception {

        when(httpRequestHelperMock.buildHttpClient()).thenReturn(httpClientMock);
        when(httpClientMock.execute(any())).thenThrow(new RuntimeException());

        weatherApiClient.callApi("TestCity");

    }

    @Test
    public void callApiReturnEmptyStringWhenApiReturnEmptyResponse() throws Exception {
        String city = "TestCity";
        prepareMocks("");

        String actualResult = weatherApiClient.callApi(city);

        verifyMethodsCalling(city);
        assertTrue(actualResult.isEmpty());
    }

    @Test
    public void callApiReturnEmptyStringWhenApiReturnNotFoundInResponse() throws Exception {
        String city = "TestCity";
        prepareMocks("NOT_FOUND");

        String actualResult = weatherApiClient.callApi(city);

        verifyMethodsCalling(city);
        assertTrue(actualResult.isEmpty());
    }

    @Test
    public void callApiReturnEmptyStringWhenApiReturnBadFormattedResponse() throws Exception {
        String city = "TestCity";
        prepareMocks("Some_bad_formatted_response");

        String actualResult = weatherApiClient.callApi(city);

        verifyMethodsCalling(city);
        assertTrue(actualResult.isEmpty());
    }

    @Test
    public void callApiReturnEmptyStringWhenApiReturnResponseWithEmptyTemperatureField() throws Exception {
        String city = "TestCity";
        prepareMocks("{\"temperature\":\"\"}");

        String actualResult = weatherApiClient.callApi(city);

        verifyMethodsCalling(city);
        assertTrue(actualResult.isEmpty());
    }

    @Test
    public void callApiReturnEmptyStringWhenApiReturnResponseWithoutTemperatureField() throws Exception {
        String city = "TestCity";
        prepareMocks("{\"field\":\"value\"}");

        String actualResult = weatherApiClient.callApi(city);

        verifyMethodsCalling(city);
        assertTrue(actualResult.isEmpty());
    }

    private void prepareMocks(String apiCallResult) throws Exception {
        when(httpRequestHelperMock.buildHttpClient()).thenReturn(httpClientMock);
        when(httpClientMock.execute(any())).thenReturn(httpResponseMock);
        when(httpRequestHelperMock.getEntity(any())).thenReturn(apiCallResult);
    }

    private void verifyMethodsCalling(String city) throws Exception {
        verify(httpRequestHelperMock).buildHttpClient();
        verify(httpClientMock).execute(httpRequestArgumentCaptor.capture());
        assertEquals("GET", httpRequestArgumentCaptor.getValue().getMethod());
        assertEquals(WeatherApiClientImpl.WEATHER_API_URL + city, httpRequestArgumentCaptor.getValue().getURI().toString());
        verify(httpRequestHelperMock).getEntity(httpResponseMock);
    }
}
